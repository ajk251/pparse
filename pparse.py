
import argparse
import os
import pathlib
import sys

import pandas as pd

#Features:
#   
#TODO:
#   stop it from adding random indexes!!!
#   excel.save won't overwrite files
#   doc strings
#   make naming more consistent
#REFACTOR:
#   make args.columns a namedtuple
#   add 'inplace' to the dfs where possible
#   lines can be consolidated
#   remove debugging print statements

#what pandas will calculate
FUNCS = ['mean', 'std', 'median', 'min', 'max', 'sum']

#------------------------------------------------
def main(args):

    #validiate 

    #if 2 args, case 1          --> case 1
    #else there must be 3       --> case 2,3,4

    #the right number of columns
    if len(args.columns) != 2 and len(args.columns) != 3:
        raise Exception('Must specifiy 2 or 3 columns only')

    #validate the csv too!!!
    make_df(args)

    #create subdirectory
    if not os.path.exists(args.path):
        os.mkdir(args.path)

    #start of the main logic - 8 possible cases (not counting -m -e [and -d -p])

    # ('no agg', 'no group')            -> nothing, maybe just excel
    #   pparse r.csv a b -n
    # ('no agg', 'group')               -> just group the files - splits them up
    #   pparse r.csv a b -n -g d e
    # ('agg', 'no group')               -> aggregate
    #   pparse r.csv a b -g d e -n
    # ('agg', 'group')                  -> group & aggregate
    #   pparse r.csv a b -g d e
    
    # ('no agg', 'no group')            -> nothing, maybe just excel
    #   pparse r.csv a b c -n
    # ('no agg', 'group')               -> just group the files - splits them up
    #   pparse r.csv a b c -n -g d e
    # ('agg', 'no group')               -> aggregate
    #   pparse r.csv a b c -g d e -n
    # ('agg', 'group')                  -> group & aggregate
    #   pparse r.csv a b c -g d e


    # the case with 2 columns and groups? 1c? 1d?

    if len(args.columns) == 2 and not args.do_agg: #works...ish
        print('1) 2 column with no aggregation - no groupby')
        #pass - nothing to do! excel?
    elif len(args.columns) == 2 and args.do_agg and not args.do_groupby:
        print('1a) 2 columns with aggregation - no groupby')
        single_results(args)
    elif len(args.columns) == 2 and args.do_agg and args.do_groupby:
        print('1b) 2 columns with aggregation - groupby')
        single_groups(args)
    elif not args.do_agg and args.do_groupby:   #works
        print('2) no aggregation - groupby')
        group_results(args)
    elif args.do_agg and not args.do_groupby:   #works
        print('3) aggregate - no groupby')
        aggregate_results(args)
    elif args.do_agg and args.do_groupby:       #works
        print('4) aggregate - groupby')
        aggregate_group(args)
    

#------------------------------------------------
def make_df(args):

    #TODO: try/catch

    #read the file
    df = pd.read_csv(args.file)

    #do the columns exist?
    columns = args.columns + args.groupby

    for col in columns:
        if col not in df.columns:
            raise Exception("Column '{}' not found in file.".format(col))

    args.df = df


def single_results(args):

    #if csv/excel
    #if metric

    #if -n, nothing to do

    df = args.df

    dfr = df.groupby(args.columns[0])

    dfr = dfr[args.columns[1]].agg(FUNCS, axis=1)
    dfr.reset_index(inplace=True)

    if args.use_excel:

        writer = pd.ExcelWriter(args.path + 'agg_results.xlsx')

        df.to_excel(writer, sheet_name='results')
        dfr.to_excel(writer, sheet_name='agg results')

        if args.metric is not None:
            df_metrics = calc_single_metric(args, dfr)
            df_metrics.to_excel(writer, sheet_name='metrics')

        writer.save()

    else:

        dfr.to_csv(args.path + 'agg_results.csv')

        if args.metric is not None:
            df_metrics = calc_single_metric(args, dfr)
            df_metrics.to_csv(args.path + 'agg_metrics.csv', index=False)


def single_groups(args):

    df = args.df
    dfgb = df.groupby(args.groupby)

    # all the results
    dfr = df.groupby(args.groupby + [args.columns[0]])
    dfr = dfr[args.columns[1]].agg(FUNCS, axis=1)
    dfr.reset_index(inplace=True)

    #if there are metrics to be calculated, calculate them
    if args.metric:

        metrics = []

        for name,group in dfgb:

            grp = group.groupby(args.columns[0])
            grp = grp[args.columns[1]].agg(FUNCS, axis=1)
            grp.reset_index(inplace=True)

            grp_metric = calc_single_metric(args, grp)
            metrics.append(grp_metric)

        dfmetrics = pd.concat(metrics)
        dfmetrics.reset_index(inplace=True)

        df_final = pd.concat([dfr, dfmetrics], axis=1)

    #write the files, with or without metrics
    if args.use_excel:
        
        writer = pd.ExcelWriter(args.path + args.prefix + 'agg_grouped_results.xlsx', engine='xlsxwriter')

        if args.metric:
            df_final.to_excel(writer, sheet_name='agg-metrics')
        else:
            dfr.to_excel(writer, sheet_name='agg-results')

        writer.save()
    else:
        if args.metric:
            df_final.to_csv(args.path + 'agg-metrics.csv', index=False)
        else:
            dfr.to_csv(args.path + 'agg-results.csv', index=False)
        

def aggregate_results(args):

    # for the case where there is only one group - the csv just needs averaged/etc...

    #write csv or make one excel sheet

    df = args.df #pd.read_csv(file, header=True)

    dfr = df.groupby([args.columns[0], args.columns[2]])
    dfr = dfr[args.columns[1]].agg(func=FUNCS, axis=1)
    dfr.reset_index(inplace=True)

    #write file
    if args.use_excel:
        writer = pd.ExcelWriter(args.path + 'agg_results.xlsx')

        df.to_excel(writer, sheet_name='results')
        dfr.to_excel(writer, sheet_name='agg results')

        if args.metric is not None:
            df_metrics = calc_metrics(args, dfr)
            df_metrics.to_excel(writer, sheet_name='agg metrics') 

        writer.save()
    else:

        dfr.to_csv(args.path + 'agg_results.csv', index=False)

        if args.metric is not None:
            df_metrics = calc_metrics(args, dfr)
            df_metrics.to_csv(args.path + 'agg_metrics.csv', index=False)
    

def aggregate_group(args):

    # splits the results into groups, then aggregates those groups

    df = args.df

    gb_columns = args.groupby + [args.columns[0], args.columns[2]]

    dfg = df.groupby(args.groupby)

    if args.use_excel:
        
        #write all results to the first sheet
        writer = pd.ExcelWriter(args.path + args.prefix + 'agg_grouped_results.xlsx', engine='xlsxwriter')
        
        temp_df = df.groupby(gb_columns)
        temp_df = temp_df[args.columns[1]].agg(FUNCS).reset_index()
        temp_df.to_excel(writer, sheet_name='aggregated groups')

        for name, group in dfg:

            grp = group.groupby([args.columns[0], args.columns[2]])
            grp = grp[args.columns[1]].agg(FUNCS).reset_index()

            grp.to_excel(writer, sheet_name=name)

            if args.metric is not None:
                df_metrics = calc_metrics(args, grp)
                df_metrics.to_excel(writer, sheet_name='metrics-' + name)

        writer.save()
    
    else:

        #write the full file
        # gb_columns +=  [args.columns[0], args.columns[2]]

        temp_df = df.groupby(gb_columns)
        temp_df = temp_df[args.columns[1]].agg(FUNCS).reset_index()
        temp_df.to_csv(args.path + args.prefix + 'agg-group_results.csv', index=False)
        
        #write the parts
        for name, group in dfg:

            grp = group.groupby([args.columns[0], args.columns[2]])
            grp = grp[args.columns[1]].agg(FUNCS).reset_index()
                
            #BUG? Name must be valid!
            grp.to_csv(args.path + args.prefix + 'agg-' + name + '.csv', index=False)

            if args.metric is not None:
                df_metrics = calc_metrics(args, grp)
                df_metrics.to_csv(args.path + 'metrics-' + name + '.csv', index=False)


def group_results(args):

    #group with no aggregation

    df = args.df
    dfg = df.groupby(args.groupby)

    #write file - 4 cases
    # if args.do_split:
    if args.use_excel:

        writer = pd.ExcelWriter(args.path + args.prefix + 'grouped_results.xlsx')
        df.to_excel(writer, sheet_name='results')

        for name, group in dfg:

            grp = group.reset_index()
            grp.to_excel(writer, sheet_name=name)

        writer.save()
    else:
        for name, group in dfg:
 
            grp = group.reset_index()

            #BUG? Name must be valid!
            grp.to_csv(args.path + args.prefix + name + '.csv', index=False)


#------------------------------------------------
# calculate metrics

def calc_single_metric(args, df_prime):

    #takes a df that has been aggregated or assumed to be, df′
    #returns one df all the metrics

    #this is a little sloppy…better way?

    #the speedup
    h3 = df_prime.loc[0, args.metric]

    df_speedup = pd.DataFrame()
    df_speedup['speedup'] = h3 / df_prime[args.metric]
    df_speedup[args.columns[0]] = df_prime[args.columns[0]]

    #efficiency
    # speedup / number-threads

    df_eff = pd.DataFrame()
    df_eff['efficiency'] = df_speedup['speedup'] / df_speedup[args.columns[0]]
    
    #karp-flatt
    #  ((1 / speedup) - (1 / threads)) / (1-(1 / threads))
    #        a                 b                   c
    a = 1.0 / df_speedup['speedup']
    b = 1.0 / df_speedup[args.columns[0]]
    c = 1.0 - b

    dfkf = (a -b) / c
    
    #make it one dataframe
    df_metrics = pd.DataFrame()
    df_metrics[args.columns[0]] = df_speedup[args.columns[0]]
    df_metrics['speedup']       = df_speedup['speedup']
    df_metrics['efficiency']    = df_eff['efficiency']
    df_metrics['karp-flatt']    = dfkf.values

    df_metrics.set_index(args.columns[0], inplace=True)

    return df_metrics


def calc_metrics(args, df_prime):

    #takes a df that has been aggregated or assumed to be, df′
    #returns one df all the metrics

    #this is a little sloppy…better way?

    #ross it
    df_pivot = df_prime.pivot(index=args.columns[0],
                              columns=args.columns[2],
                              values=args.metric)

    #the speedup
    h3 = df_pivot.iloc[0,:]
    df_speedup = h3 / df_pivot
    df_speedup.reset_index(inplace=True)
    df_speedup.set_index(args.columns[0], inplace=True)

    #efficiency
    # speedup / number-threads

    df_eff = df_speedup
    # df_eff = df_speedup.set_index(args.columns[0])
    df_eff = df_eff.divide(df_eff.index.values, axis=0)
    df_eff.reset_index(inplace=True)
    df_eff.set_index(args.columns[0], inplace=True)
    
    #karp-flatt
    #  ((1 / speedup) - (1 / threads)) / (1-(1 / threads))
    #        a                 b                   c
    a = 1.0 / df_speedup
    b = 1.0 / df_eff.index.values
    c = 1.0 - b

    df_kf = a.subtract(b, axis=0)
    df_kf = df_kf.divide(c, axis=0)
    
    # df_kf.set_index(args.columns[0], inplace=True)
    df_kf.index = df_eff.index.values
    df_kf.index.name = args.columns[0]

    #make it one dataframe
    df_speedup['metric'] = args.metric + '-speedup'
    df_eff['metric'] = args.metric + '-efficiency'
    df_kf['metric'] = args.metric + '-Karp-Flatt'

    metric = pd.concat([df_speedup, df_eff, df_kf])

    return metric


#================================================

# help from:
#   https://stackoverflow.com/questions/15753701/how-can-i-pass-a-list-as-a-command-line-argument-with-argparse
#   https://stackoverflow.com/questions/8259001/python-argparse-command-line-flags-without-arguments
#   https://xlsxwriter.readthedocs.io/example_pandas_multiple.html

parser = argparse.ArgumentParser()

parser.add_argument('file',
                     help='path and name of the csv file')
parser.add_argument('columns',
                     help='list of the number-of-threads, time, variable columns',
                     action='store',
                     default=['num-threads', 'time', 'variable'],
                     nargs='*')
parser.add_argument('-n', '--no',
                    help='skip aggregating the results',
                    action='store_false',
                    dest='do_agg',
                    default=True)
parser.add_argument('-g', '--groupby', 
                    help='list of columns to group-by the results on',
                    dest='groupby',
                    action='store',
                    default=[],
                    nargs='+')
parser.add_argument('-d', '--dir',
                    help='write file to sub-directory',
                    dest='sub_dir',
                    default='')
parser.add_argument('-p', '--prefix',
                    help='add a prefix to the file(s), ie [prefix][name].csv',
                    dest='prefix',
                    default='')
parser.add_argument('-e', '--excel',
                    help='will write all files to an excel speadsheet -- does not make csv files',
                    action='store_true',
                    dest='use_excel',
                    default=False)
parser.add_argument('-m', '--metric',
                    help='will calculate speedup, efficiency, & Karp-Flatt on [mean,median,min,max]',
                    choices=['mean', 'median', 'min', 'max'],
                    nargs='?',
                    dest='metric',
                    default=None,
                    const='mean')

args = parser.parse_args()

args.do_groupby = True if len(args.groupby) >= 1 else False
args.cw_dir = os.getcwd()                                           #the current working directory
args.os = sys.platform                                              #current platform, for use with 'others'
args.df = None                                                      #the master data frame
args.sub_dir = '/' + args.sub_dir if args.sub_dir else ''
# args.path = args.cw_dir + args.sub_dir + '/'                      #use relative paths
args.path = "." + args.sub_dir + '/'

#for debugging
print('…'*50)
print('file:      ', args.file)
print('columns:   ', args.columns)
print('groups:    ', args.groupby)
print('aggregate: ', args.do_agg)
print('groupby:   ', args.do_groupby)
print('metric:    ', args.metric)
print('sub-dir:   ', args.sub_dir)
print('prefix:    ', args.prefix)
print('excel:     ', args.use_excel)
print('cwd:       ', args.cw_dir)
print('platform:  ', sys.platform)
print('file path: ', args.path)
print('…'*50)

main(args)