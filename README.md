# pparse

A simple command-line utility to help process the results from multiple runs of processing. It will take the *results.csv* file of the output and categorize or group the results, aggregates or calculates the mean, median, std, min, or max of the runs, and calculates the speedup, efficiency, and Karp-Flatt metrics. 

Takes comma delimited csv files as input. It will output the results as a csv file or Excel file. It mostly leverages Pandas, particularly the `groupby` and `agg` methods. 

You may need to install the Python library *openpyxl* to create Excel sheets (Pandas requires it). It was written in Linux, and only modestly tested in Windows Powershell - there maybe Windows bugs lurking.

**Note:** 
<br> • It will overwrite existing files with the same name. It will never erase or modify the input csv file.
<br> • It helps to use a single string for the *group* column, eg write it to the output like 'A-B-C'.

### Useful in 2 cases:

**Note:** For simplicity, I don't use `-m` or `-e` to produce the metrics or an Excel file, or the also useful `-p` or `-d` to organize the output. To calculate the metrics, `-m` or `--metric` must be invoked. You may need to type `python` not `python3` depending on your setup.

## Case 1 *number of threads*, *n runs of the function*
```
for 1 to 12 threads:
    for 1 to 10 rounds:
        funcs...
```
results.csv looks like:

```
n-threads,rounds,time
1,1,89
1,2,86
  ⁞
1,10,93
2,1,123
2,2,127
⁞
12,1,434
12,2,445
```

use: `python3 pparse.py results.csv n-threads time`

OR, there are groups, such as schedule

```
for (schedule, chunk) in [('static', 1), ('static', 5), ... ('dynamic', 1), ...]:
    for 1 to 12 threads:
        for 1 to 10 rounds:
            omp_set_schedule(schedule, chunk)
            funcs..,
 ```

results.csv looks like:

```
schedule-chunk,n-threads,rounds,time
static-1,1,1,89
static-1,1,2,86
static-1,1,2,86
  ⁞
static-5,1,10,93
static-5,2,1,123
static-5,2,2,127
⁞
dynamic-25,12,1,434
dynamic-25,2,2,445
```
<br>use: `python3 pparse.py results.csv n-threads time -g schedule-chunk`

## Case 2 Has *number of threads*, *n rounds* of the test function, and a *variable* of interest - such as array size
```
for 1 to 12 threads:    
    for 100 to 100,000 by 100:
        for 1 to 10:
            
            funcs...
```

results.csv looks like:

```
n-threads,array-size,rounds,time
1,100,1,89
1,100,2,87
⁞
1,200,1,78
⁞
12,100,1,86
⁞
```
use: `python3 pparse.py n-threads time array-size`

Or it also has groups, such as schedule:

```
for schedule,chunk in [('static', 1), ('static', 5), ... ('dynamic', 1), ...]
    for 1 to 12 threads:    
        for 100 to 100,000 by 100:
            for 1 to 10:
                omp_set_schedule(schedule, chunk)
                funcs...
```

results.csv looks like:

```
schedule-chunk,n-threads,array-size,rounds,time
static-1,100,1,89
static-1,1,100,2,87
⁞
static-1,1,200,1,78
⁞
static-1,12,100,1,86
⁞
```
Use: 
`python3 pparse.py n-threads time array-size --group schedule-chunk`

To just split up the groups into seperate csv or sheets use with no aggregation:
`python3 pparse.py n-threads time array-size --group schedule-chunk -n`

***
To use at the command-line: <br>
    ```> python3 path/to/pparse.py results.csv threads-column time-column [variable-column] [-m] [-e] [-d] [-p] [-n]``` <br>

If there are other columns, they will simply get ignored. *pparse* only operates on the specified columns.

## The arguments are:

### help

The option `-h` or `--help` shows the options and provides a brief explanation.

### *file* [Required]

This is the output of the runs of the algorithm, as a csv file.

### *columns* [Required]

There are 2 mandatory columns: the number of threads, the time, and an optional third variable of interest, such as array size. May be used as plain-text, such as `... n-threads time-ms size` or with quotes if there are spaces or special characters, eg `… 'n-threads' ' time in ms' 'the array sizes'`. There can be no more than 3.

### -g --groupby <column1, column2...> [Groups the results based on the number of threads column and variable column]

If there are multiple categories of runs, pparse will group the results based on 'column', for example `schedule-chunk, n-threads, array-size, time`. Requires at least one column, but takes several. Outputs a csv file for each group or one Excel file with multiple sheets. 

**Note:** If there are multiple columns to groupby, concatenate them into one string for simplicity. For example, if you have *schedule* and *chunksize* use `schedule-chunk` as a single name.

### -n --no [To skip the aggregation of results]

This does not calculate the mean, std, etc of the results file. Useful when you on want divide up the groups.

### -d --dir <directory> [Puts the output into a directory]

Outputs all the results into a single directory. Useful for organization. For example, `-d g++_results` outputs to a created or existing *g++_results* directory if, for instance, testing with multiple compiler settings.

### -p --prefix [Adds a prefix for output]

Adds a prefix to all the output files. Useful for organization. For example, the output file `agg_results.csv` with `-p g++-O1_` would produce `g++-O1_agg_results.csv`.

### -e --excel [Creates an Excel file instead of a csv file]

Uses the Pandas method `to_excel` to create an Excel file. Outputs multiple sheets to a single file.

### -m --metric <statistic> [mean, median, min, max]

Calculates the speed-up, efficiency, and Karp-Flatt metric for the aggregated results or for each group of aggregated results. Will calculate each metric based on the mean if no argument is provided or on the of the listed statistics.

